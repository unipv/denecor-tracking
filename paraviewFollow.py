
'''
 Copyright 2017 Universita' degli Studi di Pavia
 Computer Vision and Multimedia Laboratory
 http://vision.unipv.it

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,

 Author: marco.piastra@unipv.it
'''


import numpy as np
import time
from paraview.simple import *
from paraview.collaboration import processServerEvents


import paraviewFollowConfig
from vicraConversions import vicra2homogeneous, hTransform

def follow(logfile):
    logfile.seek(0, 2)      # Go to the end of the file
    while True:
         line = logfile.readline()
         if not line:
             time.sleep(paraviewFollowConfig.SLEEP)    # Sleep briefly
             continue
         yield line


def getLogRecord(generator):
    logEntry = []
    while True:
        line = next(generator).strip()
        if line != '':
            logEntry.append(line)
            break
    while True:
        line = next(generator).strip()
        if line == '':
            return logEntry
        logEntry.append(line)


def parsePositionData(handleLine):
    Q0 = float(handleLine[2:8])/10000
    Qx = float(handleLine[8:14])/10000
    Qy = float(handleLine[14:20])/10000
    Qz = float(handleLine[20:26])/10000
    Tx = float(handleLine[26:33])/100
    Ty = float(handleLine[33:40])/100
    Tz = float(handleLine[40:47])/100
    return ([Q0, Qx, Qy, Qz], [Tx, Ty, Tz])

# Load pre-computed transforms
hT_u2m = np.loadtxt(paraviewFollowConfig.U2M_TRANSFORM_FILENAME)
hT_v2mri = np.loadtxt(paraviewFollowConfig.V2MRI_TRANSFORM_FILENAME)
xVersor = np.array([paraviewFollowConfig.SPHERE_DISTANCE, 0, 0])
yVersor = np.array([0, paraviewFollowConfig.SPHERE_DISTANCE, 0])
zVersor = np.array([0, 0, paraviewFollowConfig.CONE_HEIGHT / 2])
uOriginMarker = hT_u2m[0:3, 3]

# The handle to be followed in NDI Vicra's log
handleIndex = paraviewFollowConfig.VICRA_HANDLE - 1

# Connect to pvserver
Connect(paraviewFollowConfig.SERVER_NAME)

print "Connected to server."

# Create shape:
# from this point on, processServerEvents() must be called from time to time (see below)
cone = Cone()
RenameSource(paraviewFollowConfig.SOURCE_NAME + " Z", cone)
cone.Resolution = paraviewFollowConfig.CONE_RESOLUTION
cone.Radius = paraviewFollowConfig.CONE_RADIUS
cone.Height = paraviewFollowConfig.CONE_HEIGHT

sphereX = Sphere()
RenameSource(paraviewFollowConfig.SOURCE_NAME + " X", sphereX)
sphereX.Radius = paraviewFollowConfig.SPHERE_RADIUS
sphereX.PhiResolution = paraviewFollowConfig.SPHERE_RESOLUTION
sphereX.ThetaResolution = paraviewFollowConfig.CONE_RESOLUTION

sphereY = Sphere()
RenameSource(paraviewFollowConfig.SOURCE_NAME + " Y", sphereY)
sphereY.Radius = paraviewFollowConfig.SPHERE_RADIUS
sphereY.PhiResolution = paraviewFollowConfig.SPHERE_RESOLUTION
sphereY.ThetaResolution = paraviewFollowConfig.SPHERE_RESOLUTION

displayProperties = None

print "Opening log file: " + paraviewFollowConfig.INPUT_LOG_FILENAME

# Open NDI Vicra log file and mount a generator on it
fin = open(paraviewFollowConfig.INPUT_LOG_FILENAME, "r")
generator = follow(fin)

state = 0
while True:
    logEntry = getLogRecord(generator)
    if paraviewFollowConfig.SKIP_UNTIL_TIMESTAMP != "" and logEntry[0] < paraviewFollowConfig.SKIP_UNTIL_TIMESTAMP:
        continue

    if state == 0 and logEntry[0].find(paraviewFollowConfig.VICRA_COMMAND) >= 0:
        # Next log entry will contain a position reading
        state = 1
    elif state == 1:
        handleLine = logEntry[handleIndex]
        if handleIndex == 0:
            # Strip timestamp
            tokens = handleLine.split()
            handleLine = tokens[-1]
        if handleLine.find(paraviewFollowConfig.VICRA_MISSING) == -1:
            vicraPosition = parsePositionData(handleLine)
            # Limited echo to console
            if paraviewFollowConfig.ECHO:
                print handleLine
                print vicraPosition
                print
            # Update visualization
            hT_m2v = vicra2homogeneous(vicraPosition[0], vicraPosition[1])
            x = hTransform(hTransform(hTransform(xVersor, hT_u2m), hT_m2v), hT_v2mri)
            y = hTransform(hTransform(hTransform(yVersor, hT_u2m), hT_m2v), hT_v2mri)
            z = hTransform(hTransform(hTransform(zVersor, hT_u2m), hT_m2v), hT_v2mri)
            o = hTransform(hTransform(uOriginMarker, hT_m2v), hT_v2mri)
            d = z - o
            cone.Center = z
            cone.Direction = d
            sphereX.Center = x
            sphereY.Center = y
            Show(cone)
            Show(sphereX)
            Show(sphereY)
            if displayProperties is None:
                # Ensure properties
                displayProperties = GetDisplayProperties(cone)
                displayProperties.DiffuseColor = [1, 0, 0]
                displayProperties = GetDisplayProperties(sphereX)
                displayProperties.DiffuseColor = [0, 0, 1]
                displayProperties = GetDisplayProperties(sphereY)
                displayProperties.DiffuseColor = [0, 1, 0]
        else:
            Hide(cone)
            Hide(sphereX)
            Hide(sphereY)
        # Reset state
        state = 0

    # Makes sure server events are processed (otherwise everything hangs)
    processServerEvents()
