DeNeCoR Project: real-time tracking of virtual targets with ParaView
====================================================================

This software package is intended as supplementary material to:

*Matrone, G., Ramalli, A., Savoia, A., Quaglia, F., 
Castellazzi G., Morbini, P., Piastra, M.*

**An experimental protocol for assessing the performance of new ultrasound 
probes based on CMUT technology in application to brain imaging**

Submitted to [JoVE](https://www.jove.com/)

## Usage Guide

This software package shows how to visualize in real-time the position of an item 
(i.e. an Ultrasound probe in the case of the experiments performed) being tracked 
 with [NDI Polaris Vicra](http://www.ndigital.com/medical/products/polaris-family/).

The procedure described below are intended for the MS Windows operating system,
but they should be easily adaptable to other operating systems as well.

### Pre-requisites

- [Python 2.7](https://www.python.org/)
- [ParaView 5.0](http://www.paraview.org/)

The tracking software should work without particular problems even with Paraview 4.4 and above.
However, the changes introduced since ParaView 5.2 are incompatible with the software in this package. 

### Installation

```
git clone https://bitbucket.org/unipv/denecor-tracking
```

### Environment Setup


```
cd <installation directory> 
set PARAVIEW_HOME=<Root of Paraview installation>
set PYTHONPATH=%PARAVIEW_HOME%\bin;%PARAVIEW_HOME%\lib;%PARAVIEW_HOME%\lib\paraview-5.0\site-packages\vtk;%PARAVIEW_HOME%\lib\paraview-5.0\site-packages;%PYTHONPATH%
```
The above procedure should be repeated when performing each subsequent step,
as described below.

### Start ParaView server (in background)

Start a command prompt and execute the environment setup above
(ParaView server needs to run in a separate process)

Issue the following command:

```
%PARAVIEW_HOME%\bin\pvserver.exe --use-offscreen-rendering --multi-clients  
```
The server log will appear in the command window

### Start and connect ParaView client

- Start ParaView client (normal GUI mode)
- Connect to the server instance (click on the connect icon in the GUI)
- The connection should appear in the Pipeline Browser
- (A confirmation message should also appear in the server log)

### Load the virtual targets

In the ParaView client
- open the dialog File > Load State
- select the file 'virtual_targets.pvsm' provided and click 'OK'

12 white cones - in wireframe mode - should be visible in the main window

### Load the 3D MR image (DICOM)

In the ParaView client
- open the dialog File > Open
- select the directory '6_t2_spc_sag_p2_iso_1.0' provided
- select the block of .dcm files and click 'OK'
- select 'DICOM Files (Directory)'
- click on 'Apply' in the Properties pane, to load the DICOM image
- select Representation: 'Volume'

You may want to adjust both the color map and the level of transparence:

- click on 'Coloring: Edit' button to open the 'Color Map Editor'
- use the 'Choose Preset' button to select 'X Ray' standard graylevel pattern 

### Start the log follower process

Edit the file 'paraviewFollowConfig.py' and change the value of parameter 
'SERVER_NAME' to the actual server name of the host computer.

Start a command prompt and execute the environment setup above
(the log follower needs to run in a separate process)
```
python paraviewFollowConfig.py  
```
The process should enter a wait state, until the NDI Polaris Vicra Log file 
(see parameter 'INPUT_LOG_FILENAME' in file 'paraviewFollowConfig.py') is updated.


### Start the simulated log

When the NDI Polaris Vicra device is properly set and the software configuration 
is performed, a log file should record the input in textual format from the device.

This part of the procedure allows using a simple simulation that produces a log
from a pre-recorded log file (see 'logsimConfig.py' file).

Edit the file 'paraviewFollowConfig.py' and change the value of parameter 
'SERVER_NAME' to the actual server name of the host computer.

Start a command prompt and execute the environment setup above 
(the log simulator needs to run in a separate process)
```
python logsim.py  
```
The process should start copy the pre-recorded log file to a target log file
while echoing the output on the command prompt.


### Visualizing the probe position with ParaView

As long as the above simulation is run (or the actual tracked item is within
the tracker range), the position of the field of view of the probe should be
visible in real-time in the connected ParaView client.

Normal 3D commands of ParaView could be used to adjust the viewing angle and
position of the MRI image.

![Screen Sample](screen_sample.png)

### Pre-recorded data

These files contain pre-recorded data:

- 'log_poses_and_fh_MF1.txt' is a log file with the records of an actual experimental
session with a CMUT Ultrasound probe connected with NDI Polaris Vicra
- 'Calibration_MF1_1_u2m_ASCII_u2m.mat' contains a 3D transformation matrix (i.e. rotation
\+ translation) that transforms the Ultrasound Probe position to the marker position in 
the NDI Polaris Vicra field of view
-  'Acquisition_MF1_1_transformation_v2mri_ASCII_v2mri.mat' contains a 3D transformation matrix 
that transforms the NDI Polaris Vicra field of view to the space of the MRI image

### Acknowledgements

This work has been partially supported by the National governments and 
the European Union through the ENIAC JU project DeNeCoR under grant agreement
number 324257.
