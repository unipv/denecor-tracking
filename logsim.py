
'''
 Copyright 2017 Universita' degli Studi di Pavia
 Computer Vision and Multimedia Laboratory
 http://vision.unipv.it

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,

 Author: marco.piastra@unipv.it
'''


import time
import random
import logsimConfig

fin = open(logsimConfig.INPUT_LOG_FILENAME, "r")
# Make sure that no buffer is used, i.e. ensure immediate writing
fout = open(logsimConfig.OUTPUT_LOG_FILENAME, "a", 0)

while True:
    logEntry = []
    line = fin.readline().strip()
    logEntry.append(line)
    while line == '':
        line = fin.readline().strip()
        logEntry.append(line)
    # This is the first non-empty line
    while line != '':
        line = fin.readline().strip()
        logEntry.append(line)
    # Now have a complete log entry

    time.sleep(random.random() * logsimConfig.MAXSLEEP_SEC)

    # Echo to output file
    for line in logEntry:
        print >> fout, line
    fout.flush()

    # Echo to console
    for line in logEntry:
        print line

