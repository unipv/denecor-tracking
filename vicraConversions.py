
'''
 Copyright 2017 Universita' degli Studi di Pavia
 Computer Vision and Multimedia Laboratory
 http://vision.unipv.it

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,

 Author: marco.piastra@unipv.it
'''

import numpy as np


def quat2dcmVicra(q):
    # Turn q into an array
    quat = np.asarray(q)
    # Normalize
    quat /= np.linalg.norm(quat)
    # Create array
    dcm = np.zeros((3, 3))
    # Perform conversion
    dcm[0, 0] = quat[0]**2 + quat[1]**2 - quat[2]**2 - quat[3]**2
    dcm[0, 1] = 2 * (quat[1] * quat[2] + quat[0] * quat[3])
    dcm[0, 2] = 2 * (quat[1] * quat[3] - quat[0] * quat[2])
    dcm[1, 0] = 2 * (quat[1] * quat[2] - quat[0] * quat[3])
    dcm[1, 1] = quat[0]**2 - quat[1]**2 + quat[2]**2 - quat[3]**2
    dcm[1, 2] = 2 * (quat[2] * quat[3] + quat[0] * quat[1])
    dcm[2, 0] = 2 * (quat[1] * quat[3] + quat[0] * quat[2])
    dcm[2, 1] = 2 * (quat[2] * quat[3] - quat[0] * quat[1])
    dcm[2, 2] = quat[0]**2 - quat[1]**2 - quat[2]**2 + quat[3]**2
    # Transpose is Vicra-specific
    return np.transpose(dcm)


def vicra2homogeneous(q, t):
    dcm = quat2dcmVicra(q)
    t = np.array(t)
    h = np.zeros((4, 4))
    h[0:3, 0:3] = dcm
    h[0:3, 3] = t
    h[3, 3] = 1
    return h


def hTransform(v, h):
    t = np.dot(h[0:3, 0:3], v)
    t += h[0:3, 3]
    return t

def hCompose(h1, h2):
    h = np.zeros((4, 4))
    dcm = np.dot(h1[0:3, 0:3], h2[0:3, 0:3])
    t = np.dot(h2[0:3, 0:3], h1[0:3, 3]) + h1[0:3, 3]
    h[0:3, 0:3] = dcm
    h[0:3, 3] = t
    h[3, 3] = 1
    return h

